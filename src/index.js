/**
 *
 * @param data: {String, Number, Object}
 * @returns {String, Number, Object}
 */
module.exports.deafPhoneGame = function deafPhoneGame(data) {
       return data;
};

/**
 *
 * @param player1: {String}
 * @param score: {String}
 * @param player2: {String}
 * @returns {String}
 */
module.exports.nameHockeyGameWinner = function nameHockeyGameWinner(player1, score, player2) {
    let isOT = score.endsWith('OT');
    let stringOT = (isOT) ? ' in OT' : '';
    let clearScore = (isOT) ? score.replace('OT', '') : score;
    let scoreArray = clearScore.split(':');

if (scoreArray[0] > scoreArray[1]) {
	return player1 + ' defeat ' + player2 + stringOT + ' with the score '  + score;
} else {
	let newScore = scoreArray[1] + ':' + scoreArray[0];
	if (isOT) {
newScore += 'OT';
}
  return player2 + ' defeat ' + player1 + stringOT + ' with the score '  + newScore;
}
	
   
};

/**
 *
 * @param arr: {Array}
 * @param firstElementIndex: {Number}
 * @param secondElementIndex: {Number}
 * @returns {Array}
 */
module.exports.swapTwoElementsInArray = function swapTwoElementsInArray(arr, firstElementIndex, secondElementIndex) {
    if (arr[firstElementIndex] && arr[secondElementIndex]) {
let tempItem = arr[firstElementIndex];
arr[firstElementIndex] = arr[secondElementIndex];
arr[secondElementIndex] = tempItem;
return arr;
} else {
return arr;
} 
};

/**
 *
 * @param str: {String}
 * @returns {Boolean}
 */
module.exports.isBracketsBalanced = function isBracketsBalanced(str) {
let itemsArray  = [['(', ')'], ['{', '}'], ['[', ']'], ['<', '>']];
 
    let countItems = function(testString, item) {
let count = 0;
let pos = testString.indexOf(item);

while (pos !== -1) {
count++;
pos = testString.indexOf(item, pos + 1);
}

return count;

}

for (let arrItem of itemsArray) {
	 if (countItems(str, arrItem[0]) !== countItems(str, arrItem[1])) {
 return false;
}
}

return true;

};

/**
 *
 * @param x: {Object, Array}
 * @param y: {Object, Array}
 * @returns {Boolean}
 */
module.exports.deepEqual = function deepEqual(x, y) {
    if ( x === y ) return true;

  if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;

  if ( x.constructor !== y.constructor ) return false;
    
  for ( var p in x ) {
    if ( ! x.hasOwnProperty( p ) ) continue;

    if ( ! y.hasOwnProperty( p ) ) return false;
      
    if ( x[ p ] === y[ p ] ) continue;
      
    if ( typeof( x[ p ] ) !== "object" ) return false;

    if ( ! Object.equals( x[ p ],  y[ p ] ) ) return false;
  }

  for ( p in y ) {
    if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
  }
  return true;
};
